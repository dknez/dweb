---
layout: page
title: About me
subtitle: Let me introduce myself...
---

Hi, my name is Domagoj Knez and I am a Software Engineer at [FreshBooks](https://freshbooks.com).

I grew up in Zagreb, Croatia and got my BS and MS in Telecommunication and Informatics from the University of Zagreb, Faculty of Electrical Engineering and Computing.

For the last couple of years I've primarily been using Java stack but nowadays I mostly use Python and Node.js / NestJS on the backend side and React on the frontend.

This site hosts some articles I’ve written (basically some small code snippets which I might find useful some day so I just wanted to collect them all in one place).

More details on my experience, skills, projects etc. can be found on my [LinkedIn profile](https://linkedin.com/in/dknez) and on my [Projects page](/pages/projects).
