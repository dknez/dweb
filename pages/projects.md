---
layout: page
title: Projects
subtitle: More info about projects I was or am still working on.
---

## Ongoing

- **Takeaway** (April 2022 - present)

  - **Technologies**: Node.js, Javascript, Hapi, Postgres, Redis, Vue.js, Docker
  - Working on easy mobile food & drink ordering system for venues like bars, restaurants, and event spaces. When customer makes an order, pays, then they send that information to restaurants' internal systems.

- **FreshBooks** (2021 - present)

  - **Technologies**: Python, Flask, MySQL, Ruby, Ember.js, Docker, GCP, gRPC
  - Working in the API team, on various microservices: implementing reseller/referral functionalities as well as the reseller portal used for better integration of the new resellers; worked on the API standardization; worked on main microservice responsible for the client authorization and authentication.

- **KNEZ INTEH** (2021 - present)

  - **Technologies**: NestJS, React, Postgres, Docker
  - Developed and maintained a custom solution for managing the billing and documentation functionalities.

  ***

## Done

- **Telemetris** (2021)

  - **Technologies**: NestJS, React, Postgres, Docker, RabbitMQ
  - Worked in a small startup; worked on the implementation of the new smart-metering system used for tracking and metering the buoys.

- **Adcubum**: Syrius (2019 - 2021)

  - **Technologies**: Java, Spring Boot, OracleDB, Postgres, MapStruct, Spock, Gradle, Docker, Kubernetes, gRPC, Camunda BPM
  - A Java-based monolith that gives insurance companies full control over business processes. The solution covers the full range of services from contracting to payments through the BPMN process. All-in-one solution. Currently in the process of modularisation.
  - Worked in a Scrum team that created a microservice that was used for communication with the German and Swiss Insurance Associations in order to support registration of the vehicles.

- **Verso Altima Group**: Telekom Slovenije web shop and payment gateway (PGW) (2017 - 2019)

  - **Technologies**: SAP Hybris, Java, Spring, Maven, Apache Solr, REST, SOAP, Oracle, MySQL, Postgres
  - Developed a Hybris-based e-commerce solution for one of the largest Telecom operators in the region. The solution is fully integrated with the customer's Product Lifecycle Management system, together with various endpoints.
  - Lead developer on the Payment Gateway project - a Hybris extension used for managing received payments and communication with various payment providers (PayPal, WePay, Valu...).

- **mStart**: Abrakadabra webshop (2016 - 2017)

  - **Technologies**: SAP Hybris, Java, Spring, Maven, Apache Solr, REST, RabbitMQ
  - Developed a Hybris-based e-commerce solution for a new Croatian webshop [abrakadabra.com](https://abrakadabra.com).

- **Ericsson Nikola Tesla** (2013 - 2016)
  - **Technologies**: Java, Spring, JUnit, Erlang, Bash
  - Worked in the research and development department. Developed automated test environment for Radio Access Network (RAN) software. Traveled between projects in Zagreb/Stockholm.
