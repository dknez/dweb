---
layout: post
title: How to use multiple Node.js versions on your OS
subtitle: Node.js 17? Or Node.js 16? And what if you need Node.js 14? All of them?
cover-img: /assets/img/2022-03-23/nodejsCover.png
thumbnail-img: /assets/img/2022-03-23/nodejs.png
share-img: /assets/img/2022-03-23/nodejsCover.png
tags: [nodejs, node, nvm, npm]
comments: true
author:
  name: Domagoj Knez
  url: https://dknez.com
---

A couple of days ago I was forced to wipe my entire MacOS and start everything from scratch because I was too lazy to debug and try to solve a lot of different issues caused by the installation of the MDM tool used in our company.

Ok, not too big of a deal, right? Just backup the necessary documents, wipe everything, start with clean OS, install everything that is needed for everyday work (even easier when you have the bootstrap script/repo which will install a lot of stuff automatically), and continue as nothing happened.

Well, almost like that. :)
Because, in the end, I decided to manually install almost everything.

And there I am... Installed the last dependency, looking forward to finally running the project locally... But hey, the Node.j version which I have installed (the latest one) is not the one required by the project. Damn.

I remember that there was a tool that I used, something like `sdkman`; but for the love of God, I cannot remember its name at the moment.

And then I decided that I will write this post once I Google the name of this neat tool; this way I will have a note to refresh my memory if (when) this happens again.

## nvm

The tool that I used for this task is called `nvm`, a [simple yet effective tool](https://github.com/nvm-sh/nvm) which allows you to quickly install and use different Node.js versions via the command line.

### Install nvm

You can install the tool by using [Homebrew](https://brew.sh/) or by using plain-old scripts (explained in the `nvm` readme file).

I decided to go with the Homebrew, even though it is clearly stated in the already mentioned readme file that this installation is not supported. But I decided to give it a go and if I will have some issues, I will uninstall the tool installed with Homebrew and just follow the regular procedure. However, until now I had no issues.

```bash
brew install nvm
```

The setup script should automatically update your profile (either `~/.bash_profile`, `~/.zshrc`, `~/.profile`, or `~/.bashrc`).
If it failed for some reason, make sure to add the following lines to your profile:

```bash
export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh
```

Do not forget to either source the updated file (i.e. `source ~/.zshrc`) or open a new terminal window. :)

### Verify installation

If everything is set up correctly, now you should be able to run the nvm command.

You might verify your set up with commands like these:

```bash
➜  dweb git:(main) ✗ nvm --version
0.39.1
➜  dweb git:(main) ✗ nvm ls
       v14.19.0
       v15.14.0
->     v16.14.0
        v17.7.1
default -> 16 (-> v16.14.0)
iojs -> N/A (default)
unstable -> N/A (default)
node -> stable (-> v17.7.1) (default)
stable -> 17.7 (-> v17.7.1) (default)
lts/* -> lts/gallium (-> v16.14.0)
lts/argon -> v4.9.1 (-> N/A)
lts/boron -> v6.17.1 (-> N/A)
lts/carbon -> v8.17.0 (-> N/A)
lts/dubnium -> v10.24.1 (-> N/A)
lts/erbium -> v12.22.10 (-> N/A)
lts/fermium -> v14.19.0
lts/gallium -> v16.14.0
➜  dweb git:(main) ✗
```

### How to use nvm

When nvm is installed and setup, it is really easy to install, use and maintain the different Node.js versions on your system.

To download, compile and install the latest Node.js release, use this command:

```bash
nvm install node
```

A specific version can be installed by issuing this command:

```bash
nvm install 14
```

You can even explicitly define the minor/patch version as well:

```bash
nvm install 14.19.0
```

It is possible to list all the available versions of Node.js with this command:

```bash
nvm ls-remote
```

In my particular case, I have installed 4 different versions (check the output of the `nvm ls` command):

```bash
➜  dweb git:(main) ✗ nvm ls
->     v14.19.0
       v15.14.0
       v16.14.0
       v17.7.1
```

Changing the Node.js version that will be used is as easy as running this command:

```bash
nvm use v16.14.0
```

When I issue the `node --version` command, the previously defined version should be printed out in the output of this command:

```bash
➜  dweb git:(main) ✗ node --version
v16.14.0
```

And that's it.
Pretty easy, pretty simple. :)
