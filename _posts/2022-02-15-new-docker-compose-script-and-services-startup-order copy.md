---
layout: post
title: Creating new docker-compose script and dealing with services startup order
subtitle: A not-so-perfect but working solution for starting up DB, MQ and backend services
cover-img: /assets/img/2022-02-15/dockerCover.jpg
thumbnail-img: /assets/img/2022-02-15/dockerThumbnail.jpg
share-img: /assets/img/2022-02-15/dockerCover.jpg
tags: [docker, docker-compose, startup order]
comments: true
author:
  name: Domagoj Knez
  url: https://dknez.com
---

I was working on a small task for which I had to create an API service running on NestJS and which provides regular CRUD functionalities.
However, I wanted this "project" to be as simple as possible and one of the ideas was to provide the docker-compose script which would automatically create absolutely everything that is needed to be able to finally send the request to the newly created endpoints.

## How did I do it?

### Project structure

The structure of the project I was working on can be seen on the following screenshot:

![Project structure](/assets/img/2022-02-15/projectStructure.jpg){: .mx-auto.d-block :}

Besides the regular directories, it is important to note the directory named `docker` where everything related to the named `docker-compose` script is located and that there is a `Dockerfile` located in the project root directory.

### The `docker` directory

Let's start with the directory mentioned in the previous paragraph.

![Docker directory content](/assets/img/2022-02-15/dockerDirectory.jpg){: .mx-auto.d-block :}

There are three services in total we would like to start with this script: database service, MQ service and backend service (in this case it is written in NestJS).

In the `db` directory, there is a `init.sh` script that is used for database setup and initialization. Basically, it creates the database along with the new user and grants all privileges for the newly created user on this database.
This script will be referenced in the `docker-compose.yml`, as it will be seen later

```bash
#!/bin/sh
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	create database $DB_NAME;
	create user $DB_USER with encrypted password '$DB_PASS';
	grant all privileges on database $DB_NAME to $DB_USER;

	\c $DB_NAME;
	create extension if not exists "uuid-ossp";
EOSQL
```

In the `mq`directory, there are two files: `definitions.json` and `rabbitmq.conf`.

`definitions.json` is used to define the users that will be used for RabbitMQ, we define virtual hosts, permissions and define queues that will be used for publishing and retrieving messages or events.

```json
{
  "rabbit_version": "3.8.2",
  "rabbitmq_version": "3.8.2",
  "users": [
    {
      "name": "yourUsername",
      "password": "yourPassword",
      "hashing_algorithm": "rabbit_password_hashing_sha256",
      "tags": "administrator"
    }
  ],
  "vhosts": [
    {
      "name": "/"
    }
  ],
  "permissions": [
    {
      "user": "yourUsername",
      "vhost": "/",
      "configure": ".*",
      "write": ".*",
      "read": ".*"
    }
  ],
  "topic_permissions": [],
  "parameters": [],
  "global_parameters": [
    {
      "name": "cluster_name",
      "value": "rabbit@tia_mq"
    }
  ],
  "policies": [],
  "queues": [
    {
      "name": "yourQueueName",
      "vhost": "/",
      "durable": true,
      "auto_delete": false,
      "arguments": {}
    }
  ],
  "exchanges": [],
  "bindings": []
}
```

`rabbitmq.conf` file is rather simple; it has only two items:

```conf
loopback_users = none
management.load_definitions = /etc/rabbitmq/definitions.json
```

`loopback_users` item allows `guest` user to connect from a remote host.
By default, the `guest` user is prohibited from connecting from remote hosts; it can only connect over a loopback interface (`localhost`). The recommended way to address this in production systems is to create a new user or set of users with the permissions to access the necessary virtual hosts. This can be done using CLI tools, HTTP API or definitions import.
In our particular case, I decided to set this parameter to `none` as I only planned to run this app on my local machine.

Another parameter that is set up here is `management.load_definitions` and it contains the relative path to the previously mentioned `definitions.json` file.

### The `docker-compose.yml` file

And now... The `docker-compose.yml` file contains all the service definitions and their startup order.

```yml
version: '3'
services:
  db:
    image: 'postgres:13.3-alpine'
    ports:
      - 5432:5432
    volumes:
      - ./db/init.sh:/docker-entrypoint-initdb.d/init.sh
      - ./mounts/db:/var/lib/postgresql/data
    healthcheck:
      test: ['CMD-SHELL', 'pg_isready -U postgres']
      interval: 10s
      timeout: 5s
      retries: 5
  mq:
    image: 'rabbitmq:3.9-management'
    hostname: persist_it
    volumes:
      - ./mq/rabbitmq.conf:/etc/rabbitmq/rabbitmq.conf
      - ./mq/definitions.json:/etc/rabbitmq/definitions.json
      - ./mounts/mq:/var/lib/rabbitmq
    ports:
      - 15672:15672
      - 5672:5672
    healthcheck:
      test: rabbitmq-diagnostics -q ping
      interval: 30s
      timeout: 30s
      retries: 3
  backend:
    build: ../
    image: the_backend
    depends_on:
      db:
        condition: service_healthy
      mq:
        condition: service_healthy
    env_file:
      - ../.env.stage.prod
    ports:
      - 3000:3000
```

I would like to keep this rather simple and I do think that most of the settings here are basically self-explanatory so that is why I plan to focus on the parts that caused me the most headaches.
So, besides general settings, such as port mapping, volumes, etc., there are a couple of points I would like to stress out:

1. Since I did not want to publish the backend image, I decided to build the backend code. That is why in the backend service, there is a `build` option where I have defined the path to the backend code or, to be precise, the `Dockerfile` located in the project root directory. In this case, this file is located one level up.
2. Second thing that bugged me was service starting up order. Initially, while I was building this script, it would happen that `backend` service would already run even though `db` and/or `mq` services would still not be set and even though it is defined in the `docker-compose.yml` file that backend depends on db and mq. This is a part of a [much larger problem of distributed systems](https://docs.docker.com/compose/startup-order/). That is why I decided to go with the `healthcheck` option. For every service, I have created two healtchecks which will be used for checking if their parent services are up and running. Only then will the backend service finally be executed.

```yml
---
healthcheck:
  test: ['CMD-SHELL', 'pg_isready -U postgres']
  interval: 10s
  timeout: 5s
  retries: 5
---
healthcheck:
  test: rabbitmq-diagnostics -q ping
  interval: 30s
  timeout: 30s
  retries: 3
```

Note that each of these healtchecks has different commands defined with different interval/timeout/retries values defined.

### `Dockerfile` in the project root directory

This file, as mentioned, is located in the project root directory and this one is actually responsible for building the backend properly and it looks like this:

```bash
# BUILD stage
# 1) install deps and devDeps - required to produce build artifact
# 2) start Postgres and run migrations
# 3) produce build artifact
FROM postgres:13.3-alpine as builder

# postgres envs
ENV POSTGRES_USER postgresuser
ENV POSTGRES_PASSWORD postgrespassword
ENV DB_NAME dbname
ENV DB_USER dbusername
ENV DB_PASS dbpassword

COPY . /home/

RUN \
	cd home && \
	apk add --update nodejs npm && \
  npm ci && \
	npm run build

# POST BUILD stage - build artifact only needs deps (not devDeps)
FROM node:14.17-alpine as post_builder

ENV NODE_ENV production
WORKDIR /home/node

COPY package.json package-lock.json /home/node/
RUN npm ci

# RELEASE stage
FROM node:14.17-alpine
ENV NODE_ENV production
WORKDIR /home/node

COPY --from=builder /home/package*.json /home/node/
COPY --from=builder /home/dist/ /home/node/dist/
COPY --from=post_builder /home/node/node_modules/ /home/node/node_modules/

CMD ["node", "dist/main.js"]
```
