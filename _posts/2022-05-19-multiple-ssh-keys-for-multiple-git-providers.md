---
layout: post
title: Use different SSH keys with different git providers
subtitle: One possible way of using different SSH keys for multiple git providers
cover-img: /assets/img/2022-05-19/cover.jpg
thumbnail-img: /assets/img/2022-05-19/ssh.png
share-img: /assets/img/2022-05-19/cover.jpg
tags: [ssh, git, gitconfig]
comments: true
author:
  name: Domagoj Knez
  url: https://dknez.com
---

Basically, it all comes to the following: if you are working on multiple projects in different companies and you are required to use multiple GitHub/GitLab/SomethingElse accounts, how to use different SSH keys.

This is how I did it on my system.

## Directory structure

In my `~/.ssh` directory, I have two SSH keys: a private SSH key and the one used for the startup I am working for.

```
.ssh
|––– config
|––– id_rsa_pvt
|––– id_rsa_pvt.pub
|––– id_rsa_startup
|––– id_rsa_startup.pub
...
```

How to generate a new SSH key?
You can just run this command `ssh-keygen -t rsa` and follow the on-screen instructions.

Once you have generated all the needed keys, do not forget to add them to your GitHub/GitLab/SomethingElse profile.

Once this is done, it is important to update the SSH config file.

### Update `~.ssh/config`

In the `config` file, you should define which SSH key is going to be used with which domain.
For example, imagine that you have two different accounts on GitHub: one named `private` and the other named `startup`.
We want to make sure that whenever we are working on private projects which exist only under our private GitHub profile, we are using a private SSH key.
The same goes for the startup SSH key - this one should only be used when we are working on startup projects which exist under our startup GitHub profile only.

This is how I defined my config file to support the described use case:

```
Host \*
IgnoreUnknown UseKeychain
AddKeysToAgent yes
UseKeychain yes
IdentitiesOnly yes

# GitHub default

Host github.com
HostName github.com
User git
AddKeysToAgent yes
UseKeyChain yes
IdentityFile ~/.ssh/id_rsa_pvt
IdentitiesOnly yes

# GitHub startup

Host startup
HostName github.com
User git
AddKeysToAgent yes
UseKeyChain yes
IdentityFile ~/.ssh/id_rsa_startup
IdentitiesOnly yes
```

As you can see, I have defined two different hosts: I named one the github.com and named the other one startup.
What does this mean?
Well, when I working on a private project, my private SSH key will be used (`IdentityFile ~/.ssh/id_rsa_pvt`) and whenever I am working for the startup, my "startup SSH key" will be used (`IdentityFile ~/.ssh/id_rsa_startup`).

Of course, you could have defined any string for the `Host` key in the `config` file.

### Usage

So, now we have defined that my private projects are hosted on the host named `github.com` and that my startup's projects are hosted on the host named `startup`.
What does that mean?
Well, this only means that I need to be aware that the correct host is defined in my `.git/config` project's files.

For example, my startup's `.git/config` file will have `origin` defined in the following way:

```
[remote "origin"]
	url = startup:my-startup/my-project.git
	fetch = +refs/heads/*:refs/remotes/origin/*
```

And my private project has the `origin` defined in the following way:

```
[remote "origin"]
	url = git@github.com:my-username/my-project.git
	fetch = +refs/heads/*:refs/remotes/origin/*
```

#### Note

With this configuration, it is important to keep in mind that projects that are hosted on the startup's GitHub should not be accessed via github.com because this host/domain is "reserved" for my private projects.
That means that `.git/config` for the already cloned projects should be updated to reflect the changes stated above and also that when I want to clone the other project from the startup's GitHub, I should update the git clone command to:

```
git clone git@startup:my-startup/startup-project.git
```
