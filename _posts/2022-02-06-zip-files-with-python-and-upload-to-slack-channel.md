---
layout: post
title: Create password-protected zip file and upload it to Slack channel in Python
subtitle: Just a small code snippet I used to publish the generated password-protected zip file to Slack
cover-img: /assets/img/2022-02-06/slack.jpg
thumbnail-img: /assets/img/2022-02-06/slack-thumb.jpg
share-img: /assets/img/2022-02-06/slack.jpg
tags: [python, slack, encrypted zip]
comments: true
author:
  name: Domagoj Knez
  url: https://dknez.com
---

As part of a bigger task, my colleague and I were working on, we had to compress the existing `.csv` file to a password-protected `.zip` file and upload it to the predefined Slack channel.
I decided to save this small piece of code hoping it might be useful to somebody, someday, somewhere... Anyhow, the complete example can be found in [my GitLab repo](https://gitlab.com/dweb-examples/slack-encrypted-zip-report).

**Requirements**
I was using the following dependencies (defined in the `requirements.txt` file which can be used automatically installed by executing this `pip` command: `pip install -r ../requirements.txt`):

```
slackclient==2.9.3
pyminizip==0.2.6
```

## How to do it?

### Slack setup

My plan is not to go into many details regarding the Slack set up as this one should be pretty straightforward.
In general, you should do the following steps:

- Create a new workspace or use the existing one.
- Create a new app and activate `Incoming Webhooks`.
  For example, my webhooks URL was: `https://hooks.slack.com/services/someRandomData/thatIAmNot/WillingToSharePublicly`
  You can test if this endpoint is working correctly by sending the following request:
  ```bash
  curl -X POST -H 'Content-type: application/json' --data '{"text":"Hello, World!"}' https://hooks.slack.com/services/someRandomData/thatIAmNot/WillingToSharePublicly
  ```
- Define scopes for your app.
- Add app to the channel where the generated `.zip` file should be uploaded.
- Use the generated token in your python code.

### Python code

Once everything is set up properly in Slack, we can try to run the following code:

```python
import os
from datetime import date, timedelta

import pyminizip
import slack
from slack.errors import SlackApiError

SLACK_TOKEN = "replace-with-real-token"
SLACK_CHANNEL = "reports"
COMPRESSION_LEVEL = 5
ZIP_PASSWORD = "randompassword"
ZIP_FILENAME = f'./dk-report-{(date.today() - timedelta(1)).strftime("%d_%m_%Y")}.zip'
CSV_FILE = "csv/email-password-recovery-code.csv"

slack_client = slack.WebClient(token=SLACK_TOKEN)

if __name__ == "__main__":
    try:
        csvPath = os.path.join(os.getcwd(), CSV_FILE)
        pyminizip.compress(
            csvPath,
            None,
            ZIP_FILENAME,
            ZIP_PASSWORD,
            COMPRESSION_LEVEL,
        )
    except FileNotFoundError:
        print("Directory: {0} does not exist".format(csvPath))
    except NotADirectoryError:
        print("{0} is not a directory".format(csvPath))
    except PermissionError:
        print("You do not have permissions to change to {0}".format(csvPath))

    try:
        response = slack_client.files_upload(
            file=ZIP_FILENAME,
            filetype="zip",
            title=f'Daily report {(date.today() - timedelta(1)).strftime("%d_%m_%Y")}',
            channels=SLACK_CHANNEL,
        )
    except SlackApiError as e:
        assert e.response["ok"] is False
        assert e.response["error"]
        print(f"Error while uploading file to the Slack channel: {e.response['error']}")

```

Explanation here is pretty straightforward: code will look for the `.csv` file located in the `<project_root>/csv/` directory.
The found file will then be passed to pyminizip package, along with other parameters and a password-protected `.zip` file will be created in the project's root directory.

### Final result

Once the file is created, it will be published to the desired Slack channel.

![Zip file uploaded to Slack channel](/assets/img/2022-02-06/slack channel.png){: .mx-auto.d-block :}
