---
layout: post
title: How to set up multiple git profiles
subtitle: How I solved my need to work with a couple of different git users in different git repositories
cover-img: /assets/img/2022-04-15/gitCover.jpg
thumbnail-img: /assets/img/2022-04-15/git.png
share-img: /assets/img/2022-04-15/gitCover.jpg
tags: [git, gitconfig]
comments: true
author:
  name: Domagoj Knez
  url: https://dknez.com
---

After working on a couple of different projects in different startups, at one point I had to use a couple of users and ensure that `user@company1` will not mistakenly push the commit to the repository owned by `company2`.

Sure, it can be resolved by manually changing `user` and `email` value in the project's `.gitconfig` file but it is fairly easy to forget to set this value. We want everything to be automated and to work with minimal manual intervention, right?

In this post, I will present my directory structure and how I dealt with my need to define three different users for different git repositories.

## Directory structure

My directory structure looks something like this

```
Dev
|––– work
|--- pvt
     |--- privateProject1
     |--- privateProject2
     |--- myCompany
          |--- project1
```

As you can see, in my Dev directory, there are two directories: `work`and `pvt`. No need for further explanations because, I guess, directory names say it all. :)
In my private directory, there are a couple of different projects, `project1`, `privateProject1`, `privateProject2`.

I want to configure git in the following way:

- In the `work` directory, email should be set to `email@work.com`.
- In the `pvt` directory, I will mostly have my personal projects and I want all of them to use `email@private.com`
- In the `myCompany` directory, located in the `pvt`directory, I want to use third email, something like `email@startup.com`

### Update `~/.gitconfig`

(I am assuming your `.gitconfig` file exists in the default location, in your home directory)
First of all, we need to update the "main" `.gitconfig` file.
This file contains something like

```
[user]
       email = email@work.com
       name = Domagoj Knez
```

In this file, we want to add two directories that will contain new `.gitconfig` files with new email addresses.
Add the following lines to achieve this:

```
[includeIf "gitdir:~/Dev/pvt/"]
       path = ~/Dev/pvt/.gitconfig-pvt

[includeIf "gitdir:~/Development/pvt/myCompany/"]
       path = ~/Dev/pvt/myCompany/.gitconfig-mc
```

We have defined new git directories where two new `.gitconfig` files will be put and in those files, we will define other email addresses that are supposed to be used when working with these projects.
Those new config files will be saved at these paths:

- `~/Dev/pvt/.gitconfig-pvt`
- `~/Dev/pvt/myCompany/.gitconfig-mc`

It is important to note that the path stated in the `gitdir` part **should end with `/`** because we want subdirectories to be included as well.

### Create additional `.gitconfig` files

After we have defined two additional directories with custom git configuration, we can actually create these files on the defined path.
So, create `.gitconfig-pvt` in the `~/Dev/pvt/` and `.gitconfig-mc` in the `~/Dev/pvt/myCompany/`.
My `.gitconfig-pvt` file contains this content:

```
[User]
  name = Domagoj Knez
  email = email@private.com
```

and my .gitconfig-mc file contains the following content:

```
[User]
  name = Domagoj Knez
  email = email@startup.com
```

And voila, that is it.
We can now check if this really works as it is supposed to.

In the `work` directory, when I run the `git config user.email` command, I get the following output: `email@work.com`.
When I enter `pvt` directory and run the same command, I get the following output: `email@private.com`.
Also, if I enter any other subdirectory in the `pvt` **besides** `myCompany` dir, I will get the same output as well. That was achieved with that `/` that I mentioned above.
However, if I enter the `pvt/myCompany` dir/subdir and enter the same command, the output will be `email@startup.com` which means that we have definitely achieved what we wanted in the first place.

This made my life much easier because now I do not need to think about defining custom configurations for every project I am working on.
